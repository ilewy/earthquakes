//
//  ApplicationConstants.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import <Foundation/Foundation.h>

#define IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

extern NSString *const EarthquakesAPIBaseURL;
extern NSString *const EarthquakesAPILastWeekPath;

extern NSString *const TwitterAPIConsumerName;
extern NSString *const TwitterAPIConsumerKey;
extern NSString *const TwitterAPIConsumerSecret;
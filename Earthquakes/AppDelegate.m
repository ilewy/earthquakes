//
//  AppDelegate.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 05/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "AppDelegate.h"
#import <Reachability/Reachability.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [APPEARANCE styleApplication];
    
    // start notify for internet connection change
    [[Reachability reachabilityForInternetConnection] startNotifier];
    
    // setup core data with default persistant store name
    [MagicalRecord setupCoreDataStack];
    
    return YES;
}

@end

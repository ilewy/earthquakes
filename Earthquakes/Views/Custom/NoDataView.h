//
//  NoDataView.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoDataView : UIView

@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, strong) NSString *descriptionText;
@property (nonatomic, strong) UIFont *descriptionFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *contentColor UI_APPEARANCE_SELECTOR;

@property (nonatomic, assign) BOOL imageShouldColorizeWithTint;

- (void)attachToView:(UIView *)superView;
- (void)centerWithOffset:(CGPoint)offset;

@end

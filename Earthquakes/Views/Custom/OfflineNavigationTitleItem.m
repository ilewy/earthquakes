//
//  OfflineNavigationTitleItem.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import "OfflineNavigationTitleItem.h"

@interface OfflineNavigationTitleItem()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *offlineLabel;

@end

@implementation OfflineNavigationTitleItem

- (instancetype)initWithTitle:(NSString *)title {
    self = [super init];
    if (self) {
        
        self.clipsToBounds = NO;
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.text = title;
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:18.0];
        self.titleLabel.clipsToBounds = NO;
        [self.titleLabel updateFrameToFitsOneLine];
    
        self.offlineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.titleLabel.bottom, self.titleLabel.width, 10.0)];
        self.offlineLabel.textAlignment = NSTextAlignmentCenter;
        self.offlineLabel.font = [UIFont systemFontOfSize:10.0];
        self.offlineLabel.clipsToBounds = NO;
        self.offlineLabel.text = @"Offline";
        
        [self addSubview:self.titleLabel];
        [self addSubview:self.offlineLabel];
        
        self.size = self.titleLabel.size;
    }
    return self;
}

- (void)setTitleFont:(UIFont *)titleFont {
    _titleFont = titleFont;
    self.titleLabel.font = [titleFont fontWithSize:18.0];
}

- (void)setOfflineFont:(UIFont *)offlineFont {
    _offlineFont = offlineFont;
    self.offlineLabel.font = [offlineFont fontWithSize:10.0];
}

-(void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    self.titleLabel.textColor = titleColor;
    self.offlineLabel.textColor = titleColor;
}

-(void)setOffline:(BOOL)offline {
    _offline = offline;
    if (offline) {
        self.offlineLabel.text = NSLocalizedString(@"Offline", nil);
    } else {
        self.offlineLabel.text = @"Online";
    }
}

@end

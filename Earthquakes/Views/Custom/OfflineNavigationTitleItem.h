//
//  OfflineNavigationTitleItem.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import <UIKit/UIKit.h>

@interface OfflineNavigationTitleItem : UIView

@property (nonatomic, getter = isOffline) BOOL offline;
@property (nonatomic, strong) UIFont *titleFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIFont *offlineFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *titleColor UI_APPEARANCE_SELECTOR;

- (instancetype)initWithTitle:(NSString *)title;

@end

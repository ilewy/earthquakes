//
//  EarthquakeAnnotation.h
//  Earthquakes
//
//  Created by Rafał Wójcik on 07/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface EarthquakeAnnotation : NSObject <MKAnnotation>


@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) BOOL causedTsunami;

- (instancetype)initWithTitle:(NSString *)title
                     subtitle:(NSString *)subtitle
                     location:(CLLocationCoordinate2D)location;

- (MKAnnotationView *)annotationView;

@end

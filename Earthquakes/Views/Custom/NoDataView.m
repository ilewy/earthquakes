//
//  NoDataView.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "NoDataView.h"
#import <AutoLayoutKit/AutoLayoutKit.h>

@interface NoDataView()

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) UIView *superView;

@end

@implementation NoDataView

- (void)awakeFromNib {
    self.userInteractionEnabled = NO;
    self.alpha = 0.0f;
    self.imageShouldColorizeWithTint = YES;
}

- (void)attachToView:(UIView *)superView {
    _superView = superView;
    
    [superView addSubview:self];
    
    [ALKConstraints layout:self do:^(ALKConstraints *c) {
        [c make:ALKCenterX equalTo:superView s:ALKCenterX name:@"CenterInSuperViewX"];
        [c make:ALKCenterY equalTo:superView s:ALKCenterY name:@"CenterInSuperViewY"];
    }];
}

- (void)centerWithOffset:(CGPoint)offset {
    
    NSLayoutConstraint *centerX = [self.superView alk_constraintWithName:@"CenterInSuperViewX"];
    centerX.constant = offset.x;
    
    NSLayoutConstraint *centerY = [self.superView alk_constraintWithName:@"CenterInSuperViewY"];
    centerY.constant = offset.y;
}

- (void)setIconImage:(UIImage *)iconImage {
    _iconImage = iconImage;
    
    UIImageRenderingMode mode = UIImageRenderingModeAlwaysTemplate;
    if (!self.imageShouldColorizeWithTint) {
        mode = UIImageRenderingModeAlwaysOriginal;
    }
    self.iconView.image = [iconImage imageWithRenderingMode:mode];
}

- (void)setDescriptionText:(NSString *)descriptionText {
    _descriptionText = descriptionText;
    self.descriptionLabel.text = descriptionText;
}

- (void)setDescriptionFont:(UIFont *)descriptionFont {
    _descriptionFont = descriptionFont;
    self.descriptionLabel.font = descriptionFont;
}

- (void)setContentColor:(UIColor *)contentColor {
    _contentColor = contentColor;
    
    self.descriptionLabel.textColor = contentColor;
    [self.iconView setTintColor:contentColor];
}

- (void)dealloc {
    self.descriptionLabel = nil;
}

@end

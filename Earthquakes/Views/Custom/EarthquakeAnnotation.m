//
//  EarthquakeAnnotation.m
//  Earthquakes
//
//  Created by Rafał Wójcik on 07/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "EarthquakeAnnotation.h"

@implementation EarthquakeAnnotation

- (instancetype)initWithTitle:(NSString *)title subtitle:(NSString *)subtitle location:(CLLocationCoordinate2D)location {
    self = [super init];
    if (self) {
        _title = title;
        _subtitle = subtitle;
        _coordinate = location;
        _causedTsunami = YES;
    }
    return self;
}

- (MKAnnotationView *)annotationView {
    MKAnnotationView *annotationView =  [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"EarthquakeAnnotationView"];
    
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    
    if (self.causedTsunami) {
        UIImageView *tsunamiView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"TsunamiSelectedIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        
        tsunamiView.tintColor = [UIColor fromHex:0x55ACEE];
        
        annotationView.rightCalloutAccessoryView = tsunamiView;
        
        [annotationView setImage:[UIImage imageNamed:@"TsunamiSign"]];
    } else {
        [annotationView setImage:[UIImage imageNamed:@"EarthquakeSign"]];
    }
    
    return annotationView;
}

@end

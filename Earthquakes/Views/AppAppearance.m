//
//  AppAppearance.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 02.09.2014.
//
//

#import "AppAppearance.h"
#import "NoDataView.h"
#import "EarthquakeCell.h"
#import "OfflineNavigationTitleItem.h"

@interface AppAppearance ()

@end

@implementation AppAppearance

#pragma mark Singleton

+ (AppAppearance *)defaultAppearance {
    
    static AppAppearance *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[AppAppearance alloc] init];
    });
    return _sharedInstance;
}

#pragma mark Appearance methods

- (void)styleApplication {
    [self setupColors];
    [self styleStatusBar];
    [self styleNavigationBar];
    [self styleOfflineTitleBarItem];
    [self stylePullToRefresh];
    [self styleNoDataView];
    [self styleCells];
}

- (void)styleStatusBar {
    id statusbar = [UIApplication sharedApplication];
    [statusbar setStatusBarHidden:NO];
    [statusbar setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)styleNavigationBar {
    id navigationBar = [UINavigationBar appearance];
    [navigationBar setBackgroundImage:[UIImage imageFromColor:self.colorInternationalOrange] forBarMetrics:UIBarMetricsDefault];
    [navigationBar setTintColor:[UIColor whiteColor]];
    [navigationBar setShadowImage:[UIImage new]];
}

- (void)styleOfflineTitleBarItem {
    id offlineTitleBar = [OfflineNavigationTitleItem appearance];
    [offlineTitleBar setTitleFont:[self fontThinWithSize:1.0]];
    [offlineTitleBar setOfflineFont:[self fontBoldWithSize:1.0]];
    [offlineTitleBar setTitleColor:[UIColor whiteColor]];
}

- (void)stylePullToRefresh {
    id pullToRefresh = [UIRefreshControl appearance];
    [pullToRefresh setTintColor:[UIColor whiteColor]];
}

- (void)styleNoDataView {
    id noDataView = [NoDataView appearance];
    [noDataView setDescriptionFont:[self fontLightWithSize:13.0]];
    [noDataView setContentColor:self.colorAlto];
}

- (void)styleCells {
    id earthquakeCell = [EarthquakeCell appearance];
    [earthquakeCell setContentColor:[UIColor whiteColor]];
}

- (void)setupColors {
    self.colorLightningYellow = [UIColor fromHex:0xFFBE1D];
    self.colorInternationalOrange = [UIColor fromHex:0xFF5007];
    self.colorGray = [UIColor fromHex:0x7F7F7F];
    self.colorAlto = [UIColor fromHex:0xD1D1D1];
}

- (UIFont *)fontRegularWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
}
- (UIFont *)fontBoldWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize];
}
- (UIFont *)fontBoldItalicWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-BoldItalic" size:fontSize];
}
- (UIFont *)fontCondensedBlackWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:fontSize];
}
- (UIFont *)fontCondensedBoldWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-CondensedBold" size:fontSize];
}
- (UIFont *)fontItalicWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Italic" size:fontSize];
}
- (UIFont *)fontLightWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Light" size:fontSize];
}
- (UIFont *)fontLightItalicWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:fontSize];
}
- (UIFont *)fontMediumWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize];
}
- (UIFont *)fontMediumItalicWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-MediumItalic" size:fontSize];
}
- (UIFont *)fontUltraLightWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-UltraLight" size:fontSize];
}
- (UIFont *)fontUltraLightItalicWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-UltraLightItalic" size:fontSize];
}
- (UIFont *)fontThinWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Thin" size:fontSize];
}
- (UIFont *)fontThinItalicWithSize:(CGFloat)fontSize {
    return [UIFont fontWithName:@"HelveticaNeue-Thin_Italic" size:fontSize];
}



@end

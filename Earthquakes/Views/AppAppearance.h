//
//  AppAppearance.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 02.09.2014.
//
//

#import <Foundation/Foundation.h>

#define APPEARANCE [AppAppearance defaultAppearance]

@interface AppAppearance : NSObject

// Colors
@property (nonatomic, strong) UIColor *colorLightningYellow;
@property (nonatomic, strong) UIColor *colorInternationalOrange;
@property (nonatomic, strong) UIColor *colorGray;
@property (nonatomic, strong) UIColor *colorAlto;

// Singleton
+ (AppAppearance *)defaultAppearance;

// Appearance methods
- (void)styleApplication;

- (UIFont *)fontRegularWithSize:(CGFloat)fontSize;
- (UIFont *)fontBoldWithSize:(CGFloat)fontSize;
- (UIFont *)fontBoldItalicWithSize:(CGFloat)fontSize;
- (UIFont *)fontCondensedBlackWithSize:(CGFloat)fontSize;
- (UIFont *)fontCondensedBoldWithSize:(CGFloat)fontSize;
- (UIFont *)fontItalicWithSize:(CGFloat)fontSize;
- (UIFont *)fontLightWithSize:(CGFloat)fontSize;
- (UIFont *)fontLightItalicWithSize:(CGFloat)fontSize;
- (UIFont *)fontMediumWithSize:(CGFloat)fontSize;
- (UIFont *)fontMediumItalicWithSize:(CGFloat)fontSize;
- (UIFont *)fontUltraLightWithSize:(CGFloat)fontSize;
- (UIFont *)fontUltraLightItalicWithSize:(CGFloat)fontSize;
- (UIFont *)fontThinWithSize:(CGFloat)fontSize;
- (UIFont *)fontThinItalicWithSize:(CGFloat)fontSize;


@end

//
//  EarthquakeDetailHeader.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "EarthquakeDetailHeader.h"
#import "EarthquakeAnnotation.h"

@interface EarthquakeDetailHeader() <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *map;

@end

@implementation EarthquakeDetailHeader

- (void)setEarthquakeLocation:(CLLocation *)earthquakeLocation {
    _earthquakeLocation = earthquakeLocation;
    [self.map setCenterCoordinate:earthquakeLocation.coordinate];
}

- (void)updateAnnotation {
    [self.map removeAnnotations:self.map.annotations];
    
    EarthquakeAnnotation *annotation = [[EarthquakeAnnotation alloc] initWithTitle:self.placeName subtitle:self.magnitudeText location:self.earthquakeLocation.coordinate];
    annotation.causedTsunami = self.causingTsunami;
    
    [self.map addAnnotation:annotation];
}

- (void)setMap:(MKMapView *)map {
    _map = map;
    _map.delegate = self;
}

- (MKAnnotationView *) mapView: (MKMapView *) mapView viewForAnnotation:(id<MKAnnotation>) annotation
{
    if ([annotation isKindOfClass:[EarthquakeAnnotation class]]) {

        EarthquakeAnnotation *myAnnotation = (EarthquakeAnnotation *)annotation;
        
        MKAnnotationView *annotationView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"EarthquakeAnnotationView"];
        
        if (!annotationView) {
            annotationView = [myAnnotation annotationView];
        } else {
            annotationView.annotation = myAnnotation;
        }
        
        return annotationView;
    }
    return nil;
}

@end

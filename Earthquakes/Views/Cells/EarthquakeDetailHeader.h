//
//  EarthquakeDetailHeader.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface EarthquakeDetailHeader : UICollectionReusableView

@property (nonatomic, strong) NSString *placeName;
@property (nonatomic, strong) NSString *magnitudeText;
@property (nonatomic, assign) BOOL causingTsunami;

@property (nonatomic, assign) CLLocation *earthquakeLocation;

- (void)updateAnnotation;

@end

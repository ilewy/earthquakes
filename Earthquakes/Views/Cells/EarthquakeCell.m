//
//  EarthquakeCell.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 06.09.2014.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "EarthquakeCell.h"
#import <math.h>

@interface EarthquakeCell()

@property (weak, nonatomic) IBOutlet UIView *magnitudeColorView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *magnitudeColorViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UILabel *placeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *magnitudeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tsunamiImageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *labelLeadingConstraint;

@end

@implementation EarthquakeCell

- (void)setPlaceName:(NSString *)placeName {
    _placeName = placeName;
    self.placeNameLabel.text = placeName;
}

- (void)setMagnitude:(NSNumber *)magnitude {
    _magnitude = magnitude;
    float roundedValue = ceilf([magnitude floatValue] * 2.0);
    self.magnitudeColorViewWidthConstraint.constant = roundedValue;
    
    self.magnitudeLabel.text = [NSString stringWithFormat:@"%@: %.2f", NSLocalizedString(@"Magnitude", nil), [magnitude floatValue]];
}

- (void)setMagnitudeColor:(UIColor *)magnitudeColor {
    _magnitudeColor = magnitudeColor;
    self.magnitudeColorView.backgroundColor = magnitudeColor;
}

- (void)setContentColor:(UIColor *)contentColor {
    _contentColor = contentColor;
    
    self.placeNameLabel.textColor = contentColor;
    self.magnitudeLabel.textColor = contentColor;
    
    self.tsunamiImageView.tintColor = contentColor;
    
}

- (void)setEarthquakeCausingTsunami:(BOOL)earthquakeCausingTsunami {
    _earthquakeCausingTsunami = earthquakeCausingTsunami;
    
    if (earthquakeCausingTsunami) {
        self.tsunamiImageView.image = [[UIImage imageNamed:@"TsunamiSelectedIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        
        self.labelLeadingConstraint.constant = self.magnitudeLabel.left + self.tsunamiImageView.image.size.width + 5;
        
    } else {
        self.tsunamiImageView.image = nil;
        self.labelLeadingConstraint.constant = self.magnitudeLabel.left;
    }
    
}

@end

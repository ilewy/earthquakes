//
//  EarthquakeCell.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 06.09.2014.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EarthquakeCell : UICollectionViewCell

@property (nonatomic, strong) NSNumber *magnitude;
@property (nonatomic, strong) UIColor *magnitudeColor;
@property (nonatomic, strong) NSString *placeName;
@property (nonatomic, strong) UIColor *contentColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) BOOL earthquakeCausingTsunami;

@end

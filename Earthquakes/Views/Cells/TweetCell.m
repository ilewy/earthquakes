//
//  TweetCell.m
//  Earthquakes
//
//  Created by Rafał Wójcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "TweetCell.h"

@interface TweetCell()

@property (weak, nonatomic) IBOutlet UILabel *twitterUserNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tweetTextLabel;

@end

@implementation TweetCell

- (void)setTwitterUserName:(NSString *)twitterUserName {
    _twitterUserName = twitterUserName;
    self.twitterUserNameLabel.text = twitterUserName;
}

- (void)setTweetText:(NSString *)tweetText {
    _tweetText = tweetText;
    self.tweetTextLabel.text = tweetText;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.tweetTextLabel.preferredMaxLayoutWidth = self.tweetTextLabel.width;
}

@end

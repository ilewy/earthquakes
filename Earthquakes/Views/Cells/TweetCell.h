//
//  TweetCell.h
//  Earthquakes
//
//  Created by Rafał Wójcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TweetCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *twitterUserImageView;
@property (nonatomic, strong) NSString *twitterUserName;
@property (nonatomic, strong) NSString *tweetText;

@end

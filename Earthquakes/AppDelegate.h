//
//  AppDelegate.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 05/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

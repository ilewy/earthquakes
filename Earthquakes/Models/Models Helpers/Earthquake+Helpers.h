//
//  Earthquake+Helpers.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import "Earthquake.h"

@interface Earthquake (Helpers)

+ (instancetype)createFromDictionary:(NSDictionary *)dict save:(BOOL)save;

+ (instancetype)earthquakeWithID:(NSString *)identifier;

+ (void)updateFromJSONArray:(NSArray *)jsonArray;

+ (void)clearEarthquakesOlderThan:(NSInteger)days;

- (UIColor *)colorForMagnitude;

+ (NSPredicate *)predicateForEarthquakesFromLastWeekCausingTsunami:(BOOL)causingTsunami;

@end

//
//  Earthquake+Helpers.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import "Earthquake+Helpers.h"

NSString *const EarthquakesAPIFeaturePropertiesKey = @"properties";
NSString *const EarthquakesAPIFeatureGeometryKey = @"geometry";
NSString *const EarthquakeIDKey = @"id";
NSString *const EarthquakeCoordinatesKey = @"coordinates";
NSString *const EarthquakeMagnitudeKey = @"mag";
NSString *const EarthquakeMagnitudeTypeKey = @"magType";
NSString *const EarthquakePlaceKey = @"place";
NSString *const EarthquakeTsunamiKey = @"tsunami";
NSString *const EarthquakeURLKey = @"url";
NSString *const EarthquakeEventDateKey = @"time";


@implementation Earthquake (Helpers)

+ (instancetype)createFromDictionary:(NSDictionary *)dict save:(BOOL)save {
    if (![dict isKindOfClass:[NSDictionary class]]) return nil;
    
    NSString *identifier = dict[EarthquakeIDKey];
    
    Earthquake *earthquake = [self earthquakeWithID:identifier];
    
    if (!earthquake) {
        earthquake = [Earthquake createEntity];
    }
    
    earthquake.identifier = identifier;
    earthquake.longitude = dict[EarthquakesAPIFeatureGeometryKey][EarthquakeCoordinatesKey][0];
    earthquake.latitude = dict[EarthquakesAPIFeatureGeometryKey][EarthquakeCoordinatesKey][1];
    earthquake.magnitude = dict[EarthquakesAPIFeaturePropertiesKey][EarthquakeMagnitudeKey];
    earthquake.magType = dict[EarthquakesAPIFeaturePropertiesKey][EarthquakeMagnitudeTypeKey];
    earthquake.placeName = dict[EarthquakesAPIFeaturePropertiesKey][EarthquakePlaceKey];
    earthquake.url = dict[EarthquakesAPIFeaturePropertiesKey][EarthquakeURLKey];
    
    NSNumber *tsunami = NULL_TO_NIL(dict[EarthquakesAPIFeaturePropertiesKey][EarthquakeTsunamiKey]);
    if (tsunami) {
        earthquake.tsunami = tsunami;
    }
    
    NSNumber *eventTimestamp = dict[EarthquakesAPIFeaturePropertiesKey][EarthquakeEventDateKey];
    
    earthquake.eventDate = [NSDate dateWithTimeIntervalSince1970:([eventTimestamp longLongValue]/1000)];
    
    if (save) {
        [earthquake.managedObjectContext saveToPersistentStoreAndWait];
    }
    
    return earthquake;
}

+ (instancetype)earthquakeWithID:(NSString *)identifier {
    return [self findFirstWithPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", identifier]];
}

+ (void)updateFromJSONArray:(NSArray *)jsonArray {
    
    for (NSDictionary *dict in jsonArray) {
        [self createFromDictionary:dict save:NO];
    }
    
    // save all items in one time
    [[NSManagedObjectContext contextForCurrentThread] saveToPersistentStoreAndWait];
}

+ (void)clearEarthquakesOlderThan:(NSInteger)days {
    
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-(days*24*60*60)];
    
    NSArray *oldEarthquakes = [Earthquake findAllWithPredicate:[NSPredicate predicateWithFormat:@"eventDate < %@", date]];
    
    for (Earthquake *earthquake in oldEarthquakes) {
        [earthquake deleteEntity];
    }
    
    [[NSManagedObjectContext contextForCurrentThread] saveToPersistentStoreAndWait];
}

- (UIColor *)colorForMagnitude {
    
    CGFloat magnitude = [self.magnitude floatValue];
    
    NSInteger colorHEX = 0x0;
    
    if (magnitude < 0.5) {
        colorHEX = 0xff00ff;
    } else if (magnitude < 1.0) {
        colorHEX = 0xab00ff;
    } else if (magnitude < 1.5) {
        colorHEX = 0x5200fe;
    } else if (magnitude < 2.0) {
        colorHEX = 0x0033ff;
    } else if (magnitude < 2.5) {
        colorHEX = 0x0099ff;
    } else if (magnitude < 3.0) {
        colorHEX = 0x00ffff;
    } else if (magnitude < 3.5) {
        colorHEX = 0x00ffcc;
    } else if (magnitude < 4.0) {
        colorHEX = 0x00ff66;
    } else if (magnitude < 4.5) {
        colorHEX = 0x1ffa00;
    } else if (magnitude < 5.0) {
        colorHEX = 0x8ffa00;
    } else if (magnitude < 5.5) {
        colorHEX = 0xe3f900;
    } else if (magnitude < 6.0) {
        colorHEX = 0xffcc00;
    } else if (magnitude < 6.5) {
        colorHEX = 0xff7800;
    } else if (magnitude >= 6.5) {
        colorHEX = 0xff0000;
    }
    
    return [UIColor fromHex:colorHEX];
}

+ (NSPredicate *)predicateForEarthquakesFromLastWeekCausingTsunami:(BOOL)causingTsunami {
    NSDate *weekOldDate = [NSDate dateWithTimeIntervalSinceNow:-(7*24*60*60)];
    
    if (causingTsunami) {
        return [NSPredicate predicateWithFormat:@"tsunami = %@ AND eventDate > %@", @(YES), weekOldDate];
    }
    return [NSPredicate predicateWithFormat:@"eventDate > %@", weekOldDate];
}

@end

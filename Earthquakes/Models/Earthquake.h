//
//  Earthquake.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 04.09.2014.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Earthquake : NSManagedObject

@property (nonatomic, retain) NSString * identifier;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * magnitude;
@property (nonatomic, retain) NSString * magType;
@property (nonatomic, retain) NSString * placeName;
@property (nonatomic, retain) NSNumber * tsunami;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSDate * eventDate;

@end

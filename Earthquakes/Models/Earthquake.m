//
//  Earthquake.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 04.09.2014.
//
//

#import "Earthquake.h"


@implementation Earthquake

@dynamic identifier;
@dynamic latitude;
@dynamic longitude;
@dynamic magnitude;
@dynamic magType;
@dynamic placeName;
@dynamic tsunami;
@dynamic url;
@dynamic eventDate;

@end

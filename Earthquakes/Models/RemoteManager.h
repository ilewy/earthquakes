//
//  DatabaseManager.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import <Foundation/Foundation.h>
#import "Earthquake.h"
#import "Earthquake+Helpers.h"
#import "Tweet.h"
#import "TwitterUser.h"

typedef void (^GetEarthquakesSuccessBlock)(BOOL foundNew);
typedef void (^GetTweetsSuccessBlock)(NSArray *tweets);
typedef void (^RemoteManagerFailBlock)(NSError *error);

#define RMManager [RemoteManager sharedManager]

@interface RemoteManager : NSObject

+ (RemoteManager *)sharedManager;

- (void)fetchEarthquakesSuccess:(GetEarthquakesSuccessBlock)success
                      failure:(RemoteManagerFailBlock)fail;

- (void)fetchTweetsForEarthquake:(Earthquake *)earthquake
                         success:(GetTweetsSuccessBlock)success
                         failure:(RemoteManagerFailBlock)fail;

@end

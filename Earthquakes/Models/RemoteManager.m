//
//  DatabaseManager.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import "RemoteManager.h"
#import <AFNetworking/AFNetworking.h>
#import <STTwitter/STTwitter.h>

NSString *const EarthquakesAPIFeaturesKey = @"features";

@interface RemoteManager()

@property (nonatomic, strong) AFHTTPSessionManager *manager;
@property (nonatomic, strong) STTwitterAPI *twitterAPI;

@end

@implementation RemoteManager

+ (RemoteManager *)sharedManager {
    static RemoteManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[RemoteManager alloc] init];
    });
    return _sharedInstance;
}

-(AFHTTPSessionManager *)manager {
    if (!_manager) {
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:EarthquakesAPIBaseURL]];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    return _manager;
}

- (STTwitterAPI *)twitterAPI {
    if (!_twitterAPI) {
        _twitterAPI = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"rpRMV4DL6NAWrWC0DxrJPAqLJ" consumerSecret:@"2M9J16TPXbIYZerYYcjwShiok6cem4syMTdseiCQKydcdp7zlz"];
    }
    return _twitterAPI;
}

- (void)fetchEarthquakesSuccess:(GetEarthquakesSuccessBlock)success failure:(RemoteManagerFailBlock)fail {
    
    NSString *url = EarthquakesAPILastWeekPath;
    
    [[self.manager GET:url parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            // check if request found new data
            NSArray *earthquakes = responseObject[EarthquakesAPIFeaturesKey];
            
            // get all earthquakes ID-s from API
            NSArray *earthquakesIDs = [earthquakes valueForKey:@"id"];
            
            // check count of API Earthquakes in Database
            NSUInteger countOfFoundEntities = [Earthquake countOfEntitiesWithPredicate:[NSPredicate predicateWithFormat:@"identifier IN %@", earthquakesIDs]];
            
            BOOL foundNew = NO;
            
            // check if earthquakes count from api not equal count fetched from database based on ID-s
            if (countOfFoundEntities != earthquakes.count) {
                foundNew = YES;
                [Earthquake updateFromJSONArray:earthquakes];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success) { success(foundNew); }
            });
        });
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        if (fail) { fail(error); }
    }] resume];
}

- (void)fetchTweetsForEarthquake:(Earthquake *)earthquake success:(GetTweetsSuccessBlock)success failure:(RemoteManagerFailBlock)fail {
    
    @weakify(self)
    [self.twitterAPI verifyCredentialsWithSuccessBlock:^(NSString *bearerToken) {
        @strongify(self)
        
        NSString *latitude = [earthquake.latitude stringValue];
        NSString *longitude = [earthquake.longitude stringValue];
        
        NSString *geoCode = [NSString stringWithFormat:@"%@,%@,1000mi", latitude, longitude];
        
        [self.twitterAPI getSearchTweetsWithQuery:@"" geocode:geoCode lang:nil locale:nil resultType:nil count:@"20" until:nil sinceID:nil maxID:nil includeEntities:nil callback:nil successBlock:^(NSDictionary *searchMetadata, NSArray *statuses) {
            NSArray *tweets = [Tweet parseTweetsFromAPIArray:statuses];
            success(tweets);
        } errorBlock:^(NSError *error) {
            fail(error);
        }];
        
    } errorBlock:^(NSError *error) {
        fail(error);
    }];
    
    
}

@end

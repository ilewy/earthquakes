//
//  TwitterUser.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import "TwitterUser.h"

NSString *const TwitterAPITwitterUserName = @"name";
NSString *const TwitterAPITwitterUserImageURL = @"profile_image_url";

@implementation TwitterUser

- (instancetype)initFromAPIDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.name = dict[TwitterAPITwitterUserName];
        self.imageURL = dict[TwitterAPITwitterUserImageURL];
    }
    return self;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"User name: %@", self.name];
}

@end

//
//  Tweet.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import <Foundation/Foundation.h>
#import "TwitterUser.h"

@interface Tweet : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) TwitterUser *user;

- (instancetype)initFromAPIDictionary:(NSDictionary *)dict;

+ (NSArray *)parseTweetsFromAPIArray:(NSArray *)array;

@end

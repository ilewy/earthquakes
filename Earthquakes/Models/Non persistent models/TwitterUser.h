//
//  TwitterUser.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import <Foundation/Foundation.h>

@interface TwitterUser : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *imageURL;

- (instancetype)initFromAPIDictionary:(NSDictionary *)dict;

@end

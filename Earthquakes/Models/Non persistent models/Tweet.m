//
//  Tweet.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import "Tweet.h"

NSString *const TwitterAPITweetContentKey = @"text";
NSString *const TwitterAPITweetUserKey = @"user";

@implementation Tweet

- (instancetype)initFromAPIDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.text = dict[TwitterAPITweetContentKey];
        self.user = [[TwitterUser alloc] initFromAPIDictionary:dict[TwitterAPITweetUserKey]];
    }
    return self;
}

+ (NSArray *)parseTweetsFromAPIArray:(NSArray *)array {
    
    NSMutableArray *tweetsArray = [NSMutableArray arrayWithCapacity:array.count];
    for (NSDictionary *dict in array) {
        [tweetsArray addObject:[[Tweet alloc] initFromAPIDictionary:dict]];
    }
    return [tweetsArray copy];
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@: %@", self.user.name, self.text];
}

@end

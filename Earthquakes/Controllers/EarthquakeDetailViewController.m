//
//  EarthquakeDetailControllerViewController.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "EarthquakeDetailViewController.h"
#import "TweetCell.h"
#import "EarthquakeDetailHeader.h"
#import <CSStickyHeaderFlowLayout/CSStickyHeaderFlowLayout.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface EarthquakeDetailViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *tweets;
@property (nonatomic, strong) NSArray *tweetsCellHeights;

@property (nonatomic, strong) TweetCell *sizingTweetCell;

@end

@implementation EarthquakeDetailViewController

#pragma mark Controler livecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    [self requestRemoteData];
}


- (void)setupView {
    
    CSStickyHeaderFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.collectionView.width, self.collectionView.height * 0.3);
        if (deviceOrientation == UIDeviceOrientationLandscapeRight || deviceOrientation == UIDeviceOrientationLandscapeLeft) {
            layout.parallaxHeaderReferenceSize = CGSizeMake(self.collectionView.height, self.collectionView.width * 0.3);
        }
        
    }
    
    UINib *headerNib = [UINib nibWithNibName:NSStringFromClass([EarthquakeDetailHeader class]) bundle:nil];
    [self.collectionView registerNib:headerNib
          forSupplementaryViewOfKind:CSStickyHeaderParallaxHeader
                 withReuseIdentifier:NSStringFromClass([EarthquakeDetailHeader class])];
    
    self.collectionView.alwaysBounceVertical = YES;
    [self.refreshControl addTarget:self action:@selector(requestRemoteData) forControlEvents:UIControlEventValueChanged];
    
    [self.noDataView centerWithOffset:CGPointMake(0, 100)];
    
    // setup cell
    UINib *cellNIb = [UINib nibWithNibName:NSStringFromClass([TweetCell class]) bundle:nil];
    [self.collectionView registerNib:cellNIb forCellWithReuseIdentifier:NSStringFromClass([TweetCell class])];
    
    self.sizingTweetCell = [[cellNIb instantiateWithOwner:nil options:nil] objectAtIndex:0];
    
}

#pragma mark -
#pragma mark Setup data

- (void)requestRemoteData {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    @weakify(self)
    [RMManager fetchTweetsForEarthquake:self.earthquake success:^(NSArray *tweets) {
        @strongify(self)
        self.tweets = tweets;
        [self.collectionView reloadData];
        [self.refreshControl endRefreshing];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(NSError *error) {
        @strongify(self)
        [self.refreshControl endRefreshing];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

- (void)setTweets:(NSArray *)tweets {
    _tweets = tweets;
    [self calculateCellHeights];
}

#pragma mark -
#pragma mark Internet connection Changed

- (void)internetAvailable {
    [super internetAvailable];
    [self.collectionView addSubview:self.refreshControl];
}

- (void)internetNotAvaliable {
    [super internetNotAvaliable];
    [self.refreshControl removeFromSuperview];
}

#pragma mark -
#pragma mark UICollectionView delegate & datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = self.tweets.count;
    
    NSTimeInterval noDataViewDisappearAnimationTme = 0.5;
    [UIView animateWithDuration:noDataViewDisappearAnimationTme animations:^{
        if (numberOfRows == 0) {
            self.noDataView.alpha = 1.0;
        } else {
            self.noDataView.alpha = 0.0;
        }
    }];
    
    return numberOfRows;
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    TweetCell *cell = (TweetCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([TweetCell class]) forIndexPath:indexPath];
    
    Tweet *tweet = (Tweet *)self.tweets[indexPath.row];
    
    cell.twitterUserName = tweet.user.name;
    cell.tweetText = tweet.text;
    
    [cell.twitterUserImageView sd_setImageWithURL:[NSURL URLWithString:tweet.user.imageURL] placeholderImage:[UIImage imageNamed:@"TwitterProfilePlaceholder"]];
    
    return cell;
    return nil;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:CSStickyHeaderParallaxHeader]) {
        
        EarthquakeDetailHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:NSStringFromClass([EarthquakeDetailHeader class]) forIndexPath:indexPath];
        
        CLLocation *earthquakeLocation = [[CLLocation alloc] initWithLatitude:[self.earthquake.latitude doubleValue] longitude:[self.earthquake.longitude doubleValue]];
        
        header.earthquakeLocation = earthquakeLocation;
        header.placeName = self.earthquake.placeName;
        header.magnitudeText = [NSString stringWithFormat:@"%@: %.2f", NSLocalizedString(@"Magnitude", nil), [self.earthquake.magnitude floatValue]];
        header.causingTsunami = [self.earthquake.tsunami boolValue];
        [header updateAnnotation];

        return header;
    } else if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"TwitterHeader" forIndexPath:indexPath];
        header.backgroundColor = [header.backgroundColor colorWithAlphaComponent:0.8];
        return header;
    }
    return nil;
}

#pragma mark UICollectionView calculate cell heights

- (void)calculateCellHeights {
    NSMutableArray *heights = [[NSMutableArray alloc] initWithCapacity:self.tweets.count];
    
    for (Tweet *tweet in self.tweets) {
        self.sizingTweetCell.tweetText = tweet.text;
        CGSize size = [self.sizingTweetCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        [heights addObject:@(size.height)];
    }
    
    self.tweetsCellHeights = [heights copy];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *height = self.tweetsCellHeights[indexPath.row];
    return CGSizeMake(collectionView.width, [height floatValue]);
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    CSStickyHeaderFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    if ([layout isKindOfClass:[CSStickyHeaderFlowLayout class]]) {
        layout.parallaxHeaderReferenceSize = CGSizeMake(self.collectionView.width, self.collectionView.height*0.3);
    }
    [self calculateCellHeights];
    [self.collectionView performBatchUpdates:nil completion:nil];
}

@end

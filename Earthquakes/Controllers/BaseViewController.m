//
//  BaseViewController.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import "BaseViewController.h"
#import <Reachability/Reachability.h>

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.offlineTitleBar = [[OfflineNavigationTitleItem alloc] initWithTitle:self.navigationItem.title];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(internetConnectionChanged) name:kReachabilityChangedNotification object:nil];
    [self internetConnectionChanged];
}

- (void)setOfflineTitleBar:(OfflineNavigationTitleItem *)offlineTitleBar {
    _offlineTitleBar = offlineTitleBar;
    self.navigationItem.titleView = offlineTitleBar;
}

- (void)internetConnectionChanged {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        self.offlineTitleBar.offline = YES;
        [self internetNotAvaliable];
    } else {
        self.offlineTitleBar.offline = NO;
        [self internetAvailable];
    }
}

- (void)internetAvailable {
    self.noDataView.iconImage = [UIImage imageNamed:@"PullToRefreshIcon"];
    self.noDataView.descriptionText = NSLocalizedString(@"Pull to refresh data!", nil);
}

- (void)internetNotAvaliable {
    self.noDataView.iconImage = [UIImage imageNamed:@"NoInternetConnectionIcon"];
    self.noDataView.descriptionText = NSLocalizedString(@"No internet connection!", nil);
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.noDataView = nil;
    self.offlineTitleBar = nil;
    self.refreshControl = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark Lazy properties

- (UIRefreshControl *)refreshControl {
    if (!_refreshControl) {
        _refreshControl = [[UIRefreshControl alloc] init];
    }
    return _refreshControl;
}

- (NoDataView *)noDataView {
    if (!_noDataView) {
        _noDataView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([NoDataView class]) owner:nil options:nil] firstObject];
        [_noDataView attachToView:self.view];
    }
    return _noDataView;
}

@end

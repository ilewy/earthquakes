//
//  EarthquakesListViewController.m
//  Earthquakes
//
//  Created by Rafal Wojcik on 02.09.2014.
//
//

#import "EarthquakesListViewController.h"
#import "EarthquakeDetailViewController.h"
#import "EarthquakeCell.h"

@interface EarthquakesListViewController() <UICollectionViewDelegate, UICollectionViewDataSource, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *earthquakes;

@property (nonatomic, assign) BOOL showOnlyEarthquakesCausingTsunami;

@end

@implementation EarthquakesListViewController

#pragma mark Controler livecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.showOnlyEarthquakesCausingTsunami = NO;
    [self setupView];
    [self populateCollectionView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self requestRemoteData];
}

- (void)setupView {
    self.collectionView.alwaysBounceVertical = YES;
    [self.refreshControl addTarget:self action:@selector(requestRemoteData) forControlEvents:UIControlEventValueChanged];
}

#pragma mark -
#pragma mark Setup data

- (void)requestRemoteData {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    @weakify(self)
    [RMManager fetchEarthquakesSuccess:^(BOOL foundNew) {
        @strongify(self)
        [self populateCollectionView];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    } failure:^(NSError *error) {
        @strongify(self)
        [self.refreshControl endRefreshing];
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }];
}

- (void)populateCollectionView {
    
    NSPredicate *predicate = [Earthquake predicateForEarthquakesFromLastWeekCausingTsunami:self.showOnlyEarthquakesCausingTsunami];
    NSFetchRequest *request = [Earthquake requestAllSortedBy:@"magnitude" ascending:NO withPredicate:predicate];
    [request setFetchLimit:20];
    
    self.earthquakes = [[NSManagedObjectContext contextForCurrentThread] executeFetchRequest:request error:nil];
    [self.collectionView reloadData];
    [self.refreshControl endRefreshing];
}

#pragma mark -
#pragma mark Internet connection Changed

- (void)internetAvailable {
    [super internetAvailable];
    [self.collectionView addSubview:self.refreshControl];
}

- (void)internetNotAvaliable {
    [super internetNotAvaliable];
    [self.refreshControl removeFromSuperview];
}

#pragma mark - 
#pragma IBActions

- (IBAction)filterEarthquakersCausingTsunami:(UIBarButtonItem *)barButton {
    
    self.showOnlyEarthquakesCausingTsunami = !self.showOnlyEarthquakesCausingTsunami;
    [self populateCollectionView];
    
    if (self.showOnlyEarthquakesCausingTsunami) {
        [barButton setImage:[UIImage imageNamed:@"TsunamiSelectedIcon"]];
    } else {
        [barButton setImage:[UIImage imageNamed:@"TsunamiUnselectedIcon"]];
    }
    
}

#pragma mark -
#pragma mark UICollectionView delegate & datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger numberOfRows = self.earthquakes.count;
    
    NSTimeInterval noDataViewDisappearAnimationTme = 0.5;
    [UIView animateWithDuration:noDataViewDisappearAnimationTme animations:^{
        if (numberOfRows == 0) {
            self.noDataView.alpha = 1.0;
        } else {
            self.noDataView.alpha = 0.0;
        }
    }];
    
    return numberOfRows;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    EarthquakeCell *cell = (EarthquakeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"EarthquakeCell" forIndexPath:indexPath];
    
    Earthquake *earthquake = (Earthquake *)self.earthquakes[indexPath.row];
    
    cell.placeName = earthquake.placeName;
    cell.magnitude = earthquake.magnitude;
    cell.magnitudeColor = [earthquake colorForMagnitude];
    cell.earthquakeCausingTsunami = [earthquake.tsunami boolValue];
    
    return cell;
}

#pragma mark -
#pragma mark Prepare for segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EarthquakeDetailsSegue"]) {
        
        NSIndexPath *indexPathForSelectedCell = [self.collectionView indexPathForCell:sender];
        Earthquake *earthquake = (Earthquake *)self.earthquakes[indexPathForSelectedCell.row];
    
        EarthquakeDetailViewController *vc = (EarthquakeDetailViewController *)segue.destinationViewController;
        vc.earthquake = earthquake;
        
    }
}

@end

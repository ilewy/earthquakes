//
//  BaseViewController.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 03.09.2014.
//
//

#import <UIKit/UIKit.h>
#import "NoDataView.h"
#import "OfflineNavigationTitleItem.h"

@protocol InternetConnectionChangedProtocol <NSObject>

@optional
-(void)internetAvailable;
-(void)internetNotAvaliable;

@end

@interface BaseViewController : UIViewController <InternetConnectionChangedProtocol>

@property (nonatomic, strong) OfflineNavigationTitleItem *offlineTitleBar;
@property (nonatomic, weak) NoDataView *noDataView;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

// If you override this method, you must call super at some point in your implementation.
- (void)internetAvailable;

// If you override this method, you must call super at some point in your implementation.
- (void)internetNotAvaliable;

@end

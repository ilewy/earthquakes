//
//  EarthquakeDetailControllerViewController.h
//  Earthquakes
//
//  Created by Rafal Wojcik on 06/09/14.
//  Copyright (c) 2014 Chillicoders. All rights reserved.
//

#import "BaseViewController.h"

@interface EarthquakeDetailViewController : BaseViewController

@property (nonatomic, strong) Earthquake *earthquake;

@end
